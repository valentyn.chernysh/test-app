import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { AppContentHeaderComponent } from './components/content-header/content-header.component';
import { AppContentMenuComponent } from './components/content-menu/content-menu.component';
import { AppLeftHeaderComponent } from './components/left-header/left-header.component';
import { AppLeftMenuComponent } from './components/left-menu/left-menu.component';
import { AppLessonMenuComponent } from './components/lesson-menu/lesson-menu.component';
import { AppUserInfoComponent } from './components/user-info/user-info.component';
import { AppUsersMenuComponent } from './components/users-menu/users-menu.component';

import { ObjectTransform } from './pipes/object-transform.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AppContentHeaderComponent,
    AppContentMenuComponent,
    AppLeftHeaderComponent,
    AppLeftMenuComponent,
    AppLessonMenuComponent,
    AppUserInfoComponent,
    AppUsersMenuComponent,
    ObjectTransform
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [
    AppComponent,
    AppContentHeaderComponent,
    AppContentMenuComponent,
    AppLeftHeaderComponent,
    AppLeftMenuComponent,
    AppLessonMenuComponent,
    AppUserInfoComponent,
    AppUsersMenuComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
