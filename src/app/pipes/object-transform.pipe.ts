import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'objectTransform'})
export class ObjectTransform implements PipeTransform {
  transform(value, args: string[]): any {
    const values = [];
    for (const key in value) {
      if (key) {
        values.push(value[key]);
      }
    }
    return values;
  }
}
