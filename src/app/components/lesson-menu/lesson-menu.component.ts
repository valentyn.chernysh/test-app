import { Component } from '@angular/core';

@Component({
  selector: 'app-lesson-menu-component',
  templateUrl: './tmpl/lesson-menu.html',
  styleUrls: ['./less/lesson-menu.less']
})

export class AppLessonMenuComponent {
  public items: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
}
