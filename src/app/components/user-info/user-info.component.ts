import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-info-component',
  templateUrl: './tmpl/user-info.html',
  styleUrls: ['./less/user-info.less']
})

export class AppUserInfoComponent {

  @Input()
  userInfo: any;
  @Input()
  userId: any;
  @Input()
  setUserScore: any;

  public buttonsSet: any = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

  getMainScore = (userInfo) => {
    let mainScore = 0,
      themesLength = userInfo.assessments.length,
      index = 0;
      for(index; index < themesLength; index++) {
        mainScore += userInfo.assessments[index].value / 10;
      }
    return mainScore.toFixed(1);
  }

}
