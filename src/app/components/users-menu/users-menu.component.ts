import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-users-menu-component',
  templateUrl: './tmpl/users-menu.html',
  styleUrls: ['./less/users-menu.less']
})

export class AppUsersMenuComponent {

  @Input()
  openUserProfile: any;
  @Input()
  users: any;

}
