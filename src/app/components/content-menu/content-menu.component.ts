import { Component } from '@angular/core';

import { USERS } from '../../../assets/jsons/users';

@Component({
  selector: 'app-content-menu-component',
  templateUrl: './tmpl/content-menu.html',
  styleUrls: ['./less/content-menu.less']
})

export class AppContentMenuComponent {
  public isOpenUserProfile: boolean = false;
  public users: any = [];
  public userInfo: any = {};
  public userId: any = '0';

  constructor() {
    this.users = USERS;
    console.log("users: ", this.users);
  }

  openUserProfile = (userInfo: any, userIndex: any) => {
    console.log("userIndex: ", userIndex);
    this.userInfo = userInfo;
    if(!this.userInfo.score) {
      this.userInfo.score = [];
    }
    this.userId = userIndex;
    this.isOpenUserProfile = true;
  }

  setUserScore = (score: string, userId: any) => {
    try {
      // if(this.users[userId].score && this.users[userId].score.length > 0) {
      //   this.users[userId].score.push(score);
      // } else {
      //   this.users[userId].score = [score];
      // }
      this.users[userId].assessments.splice(0, 1);
      this.users[userId].assessments.push({date: "test date", theme: "test theme", value: score});
      this.users[userId].isScore = true;
    } catch {
      console.log("No such element in users object");
    } finally {
      this.isOpenUserProfile = false;
    }
  }
}
