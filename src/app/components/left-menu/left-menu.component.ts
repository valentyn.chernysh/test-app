import { Component } from '@angular/core';

@Component({
  selector: 'app-left-menu-component',
  templateUrl: './tmpl/left-menu.html',
  styleUrls: ['./less/left-menu.less']
})

export class AppLeftMenuComponent {
  title = 'app';
}
